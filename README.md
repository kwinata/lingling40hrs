# LingLing40hrs!

![Follow your heart](follow_your_heart.jpg)


[Guitar LingLing40hrs](guitar/)

## Suzuki Piano 3

### Sonatina Op 36 No 1, Clementi

![](static/clementi-36-1.mp4)

### Sonatina Op 55 No 1, Kuhlau

![](static/kuhlau-55-1.mp4)

- 1st Movement - 2020-11-15 ![](static/kuhlau-55-1-1stMov-2020-11-15.mp3)
- 2nd Movement - 2020-11-18 ![](static/kuhlau-55-1-2ndMov-2020-11-18.mp3)

### Theme Arr. Beethoven

![](static/theme-beethoven.mp4)

### The Wild Rider, Schumann

![](static/wild-rider-analysis.mp4)

### Ecossaise, Beethoven

![](static/ecossaise-beethoven.mp4)

### Sonatina Op 36 No 3, Spiritoso, Clementi

![](static/clementi-36-3-spiritoso.mp4)

### Sonatina, Mozart

![](static/mozart-sonatina.mp4)


## Suzuki Piano 4

### Rondo, from Divertimento in D major, K. 334 (M. A. Mozart)

![](static/rondo-334-mozart.mp4)

### Minuet I, from 8 Minuets, K. 315g (M. A. Mozart)

![](static/minuet-1-mozart.mp4)

### Minuet III, from 8 Minuets, K. 315g (M. A. Mozart)

![](static/minuet-3-mozart.mp4)

### Minuet VIII, from 8 Minuets, K. 315g (M. A. Mozart)

![](static/minuet-8-mozart.mp4)

### Musette in D Major, BWV Anh. II 126, from J. S. Bach's Notebook for Anna Magdalena Bach  (Anonymous) 

![](static/musette-126-bach.mp4)

### Sonata in G Major, Op. 49, No. 2 (6:04 Allegro, ma non troppo, 10:50 Tempo di minuetto) (L. van Beethoven) 

![](static/sonata-49-2-beethoven.mp4)

### Gavotte, from Suite in G Minor, BWV 822 (J. S. Bach)

![](static/gavotte-822-bach.mp4)

