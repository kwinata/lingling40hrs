# Guitar LingLing40hrs!

## Suzuki Guitar 2

### 1. long-long-ago

![](static/2-1-long-long-ago.mp4)

### 2. allegro

![](static/2-2-allegro.mp4)

### 3. a-toye

![](static/2-3-a-toye.mp4)

### 4. andante

![](static/2-4-andante.mp4)

### 5. andante-paganini

![](static/2-5-andante-paganini.mp4)

### 6. allegreto-giuliani

![](static/2-6-allegreto-giuliani.mp4)

### 7. corrente-paganini

![](static/2-7-corrente-paganini.mp4)

### 8. andantino-carcassi

![](static/2-8-andantino-carcassi.mp4)

### 9. allegretto-carulli

![](static/2-9-allegretto-carulli.mp4)

### 10. waltz-calatayud

![](static/2-10-waltz-calatayud.mp4)

## Suzuki Guitar 3

### 1. nonesuch-anonymous

![](static/3-1-nonesuch-anonymous.mp4)

### 2. greensleeves-anonymous

![](static/3-2-greensleeves-anonymous.mp4)

### 3. packingtons pound-anonymous

![](static/3-3-packingtons-pound-anonymous.mp4)

### 4. ghiribizzo-paganini

![](static/3-4-ghiribizzo-paganini.mp4)

### 5. waltz-paganini

![](static/3-5-waltz-paganini.mp4)

### 6. andantino-carulli

![](static/3-6-andantino-carulli.mp4)

### 7. calliope-sagreras

![](static/3-7-calliope-sagreras.mp4)

### 8. etude-carulli

![](static/3-8-etude-carulli.mp4)

### 9. etude-coste

![](static/3-9-etude-coste.mp4)

### 10. arietta-kuffner

![](static/3-10-arietta-kuffner.mp4)

### 11. celeste y Blanco-ayala

![](static/3-11-celeste-y-blanco-ayala.mp4)

## Suzuki Guitar 4

### 1. siciliana-carcassi

![](static/4-1-siciliana-carcassi.mp4)

### 2. allegro-giuliani

![](static/4-2-allegro-giuliani.mp4)

### 3. lesson-f-sor

![](static/4-3-lesson-f-sor.mp4)

### 4. etude-op-60-no-9-f-sor

![](static/4-4-etude-op-60-no-9-f-sor.mp4)

### 5. waltz-meissonnier

![](static/4-5-waltz-meissonnier.mp4)

### 6. waltz-allegro-carcassi

![](static/4-6-waltz-allegro-carcassi.mp4)

### 7. lesson-for-two-lutes-guitar-one

![](static/4-7-lesson-for-two-lutes-guitar-one.mp4)

### 7. lesson-for-two-lutes-guitar-two

![](static/4-7-lesson-for-two-lutes-guitar-two.mp4)

### 8. bouree-mozart

![](static/4-8-bouree-mozart.mp4)

### 9. la-folia-vidali

![](static/4-9-la-folia-vidali.mp4)

